##Program that gueses a person's birthday##


from random import randint

greetings = input("Hi! What is your name?\n")


#For loop to guess 5 times then give up if 5 failed guesses occur
for guess in range(1, 6):
    birthmonth2 = randint(1,12)
    birthyear = randint(1924,2004)
    answer = input("Hi " + str(greetings) + "! " + "Were you born in " + str(birthmonth2) + "/" + str(birthyear) + "?")
    if answer == "Yes":
        print("Yes! I knew it!")
        exit()
    elif guess == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
